#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "header.h"


list *create_list(){
  list *l = (list*)malloc(sizeof(list));
  l->begin = l->end = NULL;
  l->size = 0;
  return l;
}

node *new_node(double x,double y,double z){
  node *temp = (node*)malloc(sizeof(node));
  temp->next = NULL;
  temp->x = x;
  temp->y = y;
  temp->z = z;
  return temp;
}

void insert_list(list *l,double x,double y,double z){
  node *new = new_node(x,y,z);
  new->index = linked_list.l->size;
  l->size++;
  if(l->begin == NULL){//Primeira inserção;
    l->begin = new;
  }
  else{
    l->end->next = new;
  }
  l->end = new;
}

// Takes head pointer of 
// the linked list and index
// as arguments and return
// data at index
int GetNth(list *l, int index){
     
    node* current = linked_list.l->begin;
     
     // the index of the 
     // node we're currently
     // looking at
    int count = 0;
    while (current != NULL){
        if (count == index){
          printf("x:%lf\n",current->x);
          printf("y:%lf\n",current->y);
          printf("z:%lf\n",current->z);
          return(current->index);
        }

        count++;
        current = current->next;
    }
 
    /* if we get to this line, 
       the caller was asking
       for a non-existent element
       so we assert fail */
    assert(0);             
}

void print_list(list *l){
  node *temp = l->begin;

  if(temp == NULL){
    printf("list empty\n");
    return;
  }
  while(temp != NULL){
    printf("x: %lf y: %lf z: %lf", temp->x,temp->y,temp->z);
    printf("\n");
    temp = temp->next;
  }
}

//funcao que le um arquivo e adiciona os dados numa lista
void read_archive(char *name_archive){

  FILE *ent;
  int i=1;
  node temp;
  //garantir que a string de tamanho 2 SNo.tipo é terminada nula
  // temp.tipo[1]='\0';
  ent = fopen(name_archive,"r");
  if(ent == NULL){
    printf("Check the file name and path!\n");
    exit(-1);
  }
  else{
    printf("\nFile successfully read!\n\n");
  }
  char buff[256];
  //para cada linha do arquivo até o final
  while((fgets(buff,256,ent))){
    if(strlen(buff)==255){
      printf("Error, the line of a file can not exceed 256 characters\n");
      exit(-1);
    }
    //captura uma string no max 30 caracteres, um inteiro, um caracter, outra string max 8 chars, e um float
		int res=sscanf(buff,"%lf,%lf,%lf",&temp.x,&temp.y,&temp.z);
		//se o scanf capturar 5 variávei, ou seja, a linha tem um formato válido
		if(res==3){
      insert_list(linked_list.l,temp.x,temp.y,temp.z);
		}
    else{
      printf("line% i of the file has an incorrect format and was ignored\n",i);
    }
    i++;
    }
    fclose(ent);
}